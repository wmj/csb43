<!--
SPDX-FileCopyrightText: 2024 wmj <wmj.py@gmx.com>

SPDX-License-Identifier: LGPL-3.0-or-later
-->

## Installing

Basic functionality (conversion to json, homebank and OFX)

    $ pip install csb43


Conversion to YAML

    $ pip install csb43[yaml]


Conversion to basic Tablib formats

    $ pip install csb43[basic_formats]


Conversion to all Tablib formats

    $ pip install csb43[formats]

Conversion to all supported formats

    $ pip install csb43[all]


