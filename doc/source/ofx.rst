.. SPDX-FileCopyrightText: 2024 <wmj.py@gmx.com>
..
.. SPDX-License-Identifier: LGPL-3.0-or-later

.. csb43 Ofx

.. contents:: :local:

:mod:`csb43.ofx`
=================

.. automodule:: csb43.ofx
    :members:
    :inherited-members:

Converter
----------

.. automodule:: csb43.ofx.converter
    :members:

