.. SPDX-FileCopyrightText: 2024 <wmj.py@gmx.com>
..
.. SPDX-License-Identifier: LGPL-3.0-or-later

.. csb43 Utils

:mod:`csb43.utils`
======================

.. automodule:: csb43.utils
    :members:

.. automodule:: csb43.utils.currency
    :members:

.. automodule:: csb43.utils.tabulated
    :members: