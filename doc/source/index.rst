.. SPDX-FileCopyrightText: 2024 <wmj.py@gmx.com>
..
.. SPDX-License-Identifier: LGPL-3.0-or-later

.. csb43 documentation master file, created by

Welcome to csb43's documentation!
=================================

.. automodule:: csb43

API
====

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    csb
    ofx
    homebank
    formats
    utils
    records

.. include:: converter.rst

Indices and tables
===================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

