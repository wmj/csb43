.. SPDX-FileCopyrightText: 2024 <wmj.py@gmx.com>
..
.. SPDX-License-Identifier: LGPL-3.0-or-later

.. csb43 Homebank

:mod:`csb43.homebank`
======================

.. automodule:: csb43.homebank
    :members:


Converter
----------

.. automodule:: csb43.homebank.converter
    :members:

