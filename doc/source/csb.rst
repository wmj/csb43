.. SPDX-FileCopyrightText: 2024 <wmj.py@gmx.com>
..
.. SPDX-License-Identifier: LGPL-3.0-or-later

.. csb43 API Documentation

.. contents:: :local:

:mod:`csb43.aeb43`
====================

.. automodule:: csb43.aeb43
    :members:


Context -- [en] Contexto
------------------------

.. automodule:: csb43.aeb43.record.context
    :members: