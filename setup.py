#!/usr/bin/env pytho

# SPDX-FileCopyrightText: 2024 wmj <wmj.py@gmx.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import setuptools

setuptools.setup()
